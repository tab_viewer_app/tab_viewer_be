const User = require('../models/User');
const encrypter = require('../tools/encrypter');
const jwt = require('jsonwebtoken');

register = async function (req, res) {
    const username = req.body.username;
    const password = req.body.password;
    const email = req.body.email;
    const accountname = req.body.accountname;

    getUserByUsername(username, (err, exists) => {
        if (err) throw err;
        else if (exists) {
            return res.json({
                success: false,
                payload: {
                    message: 'That username is already taken! Please pick another one'
                }
            });
        }

        getUserByEmail(email, (err, exists) => {
            if (err) throw err;
            else if (exists) {
                return res.json({
                    success: false,
                    payload: {
                        message: 'That email already has an account associated.'
                    }
                });
            }

            encrypter.hashPassword(password, (err, hash) => {
                if (hash) {
                    const newUser = new User({
                        username: username,
                        email: email,
                        password: hash,
                        accountname: accountname
                    });
    
                    newUser.save((err, response) => {
                        if (err) {
                            return res.json({
                                success: false,
                                payload: {
                                    message: 'Something happened while trying to save your account! Please try again.'
                                }
                            });
                        };
    
                        res.json({
                            success: true,
                            payload: {
                                user: {
                                    username: response.username,
                                    password: response.password
                                }
                            }
                        });
                    });
                } else {
                    res.json({
                        success: false,
                        payload: {
                            message: 'Something happened while trying to save your account! Please try again.'
                        }
                    });
                }
            });
        });
    });
}

login = function (req, res, next) {
    const email = req.body.email;
    const password = req.body.password;

    getUserByEmail(email, (err, user) => {
        if (err) throw err;
        if (!user) {
            return res.json({
                success: false,
                payload: {
                    message: 'Wrong credentials'
                }
            });
        }

        console.log(user);

        encrypter.comparePassword(password, user.password, (err, isMatch) => {
            if (isMatch) {
                //Expires in 1 day
                const token = jwt.sign(user.toJSON(), process.env.JWT_SECRET, {
                    expiresIn: 86400
                });

                res.json({
                    success: true,
                    payload: {
                        token: 'JWT ' + token,
                        user: {
                            id: user._id,
                            username: user.username,
                            email: user.email,
                            accountname: user.accountname
                        }
                    }
                });
            } else {
                res.json({
                    success: false,
                    payload: {
                        message: 'Wrong credentials'
                    }
                });
            }
        });
    });
}

getUserByUsername = async (username, callback) => {
    User.findOne({
        username
    }, callback);
}

getUserByEmail = async (email, callback) => {
    User.findOne({
        email
    }, callback);
}

module.exports = {
    register,
    login
}