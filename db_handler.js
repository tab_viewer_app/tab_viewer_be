require('dotenv').load();
const mongoose = require('mongoose');

const database = process.env.DB_PROD;
const username = process.env.DB_USER;
const password = process.env.DB_PASSWORD;
const mongoDB = `mongodb+srv://${username}:${password}@cluster0-ehnwi.mongodb.net/${database}?retryWrites=true`;

mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));