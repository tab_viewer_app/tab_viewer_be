const mongoose = require('mongoose');
const Schema = mongoose.Schema;

class User {
    static schema() {
        return new Schema({
            username: {type: String, required: true, max: 30},
            email: {type: String, required: true, max: 50},
            password: {type: String, required: true, min: 6},
            accountname: {type: String, required: false, max: 100}
        });
    }
}

module.exports = mongoose.model('User', User.schema());