const express = require('express');
const router = express.Router();
const passport = require('passport');
require('../config/passport')(passport);

const controller = require('../controllers/account.controller');

router.post('/login', controller.login);
router.post('/register', controller.register);
//router.get('/profile', passport.authenticate('jwt', {session: false}), controller.profile);

module.exports = router;