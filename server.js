require('dotenv').load();
require('./db_handler');
const passport = require('passport');
let express = require('express');
let bodyParser = require('body-parser');
let app = express();

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use( (req, res, next) => {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    
    next();
});

//Passport Middleware
app.use(passport.initialize());
app.use(passport.session());

const account = require('./routes/account.route');

app.use('/account',account);

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log('TabViewer server listening on port 4000!');
});