let bcrypt = require('bcrypt');

const saltRounds = 10;

const hashPassword = function(password, callback) {
    bcrypt.hash(password, saltRounds, function(err, hash) {
        if(err) throw err;
        callback(null,hash);
    });
}

const comparePassword = function(password, hash, callback) {
    bcrypt.compare(password, hash, function(err, isMatch) {
        if(err) throw err;
        callback(null,isMatch);
    });
}

module.exports = {
    hashPassword,
    comparePassword
}